<?php
	session_start();
	include("db_conn.php");
?>

<html>
<head>
<meta charset="utf-8">
</head>
<body>
<h2> Cerca per squadra </h2>

 <form name= "squadra" action="table.php" method="post">
  <select name="team">
  <option value="">Select a team:</option>
  <?php
    // Query
    $sql_query = "SELECT name FROM team ORDER BY name";
    // Execute query
    $result = pg_query($sql_query) or die('Query faild: ' .pg_last_error());

    while ($row = pg_fetch_array($result)){
    echo '<option value="'. $row['name'] . '">'. $row['name'] . '</option>';
    }
  ?>
  </select>
  <select name="year">
  <option value="">Select a Starting Year: </option>
  <?php
    // Query
    $sql_query = "SELECT startingyearseason FROM team GROUP BY startingyearseason";
    // Execute query
    $result = pg_query($sql_query) or die('Query faild: ' .pg_last_error());

    while ($row = pg_fetch_array($result)){
    echo '<option value='. $row['startingyearseason'] . '>'. $row['startingyearseason'] . '</option>';
    }
  ?>
  </select>
  <button> Cerca </button>
  </form>


<h2> Cerca per giocatore </h2>
  <form name="cane" action="pie.php" method="post">
  	<select name="player">
  	<option value="">Select a player:</option>
  	<?php


  	$q1 = "SELECT username FROM player";

  	$result = pg_query($q1) or die('Query faild: ' .pg_last_error());

  	while ($row = pg_fetch_array($result)){
    echo '<option value="'. $row['username'] . '">'. $row['username'] . '</option>';
    }
  ?>
  </select>
  <button> Cerca </button>
  </form>

</body>
</html>