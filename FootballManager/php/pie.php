<?php

  include("db_conn.php");


      $q = "SELECT drill.mainobjective AS obj, SUM(drill.duration) AS tot_min, ROUND ( SUM(drill.duration)*100.0/SUM(SUM(drill.duration)) over(), 2 )  AS percentage
    FROM attend1
      INNER JOIN drill
        ON attend1.iddrill = drill.iddrill
        INNER JOIN player
          ON player.username = attend1.usernameplayer
    WHERE player.username = '".$_POST["player"]."'
    GROUP BY drill.mainobjective
    ORDER BY percentage DESC";
    
    $res = pg_query($q) or die('fottiti: ' .pg_last_error());
?>

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Objective', 'Percentage'],

            <?php
            while($row = pg_fetch_array($res))
            {
              echo "['".$row["obj"]."', ".$row["percentage"]." ],";
            }
            ?>
        ]);

        var options = {
          title: 'Objective of single player'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
  </body>
</html>