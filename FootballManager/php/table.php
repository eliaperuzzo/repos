<?php

  include("db_conn.php");


  $q = "SELECT name, surname
        FROM player 
        INNER JOIN appertain
          ON  appertain.usernameplayer = player.username
        WHERE  appertain.nameteam = '".$_POST["team"]."' AND appertain.startingyearteam = '".$_POST["year"]."'
        ORDER BY federationid;";

  $result = pg_query($q) or die('Query faild ' .pg_last_error());
?>

<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Surname');
        data.addRows([
            ['Cane', 'Secco'],
            <?php
            while($row = pg_fetch_array($result))
            {
              echo "[ '".$row["name"]."' , '".$row["surname"]."' ],";
            }
            ?>
        ]);

        var table = new google.visualization.Table(document.getElementById('table_div'));

        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
    </script>
  </head>
  <body>
    <div id="table_div"></div>
  </body>
</html>
